from dophon import docker_boot

docker_boot.run_as_docker(
    'Bootstrap.py',
    # docker_remote_host='',
    # attach_cmd=True,
    package_repository='https://mirrors.aliyun.com/pypi/simple/',
    extra_package={
        # 'dophon': '*'
    },
    # package_cache_path='site-packages',
    # volumes={
    #     '~/beans': '~/beans',
    #     '~/routes': '~/routes',
    #     '/home': '~/beans',
    #     '/test_dir': '/test_dir'
    # }
)
