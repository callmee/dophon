from dophon import webboot
from dophon import cluster_boot


def run():
    cluster_boot.run_clusters(webboot, 3, True, multi_static_fix=True)


run()
